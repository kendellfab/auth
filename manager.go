package auth

import "net/http"

type Manager interface {
	DoLogin(w http.ResponseWriter, r *http.Request, token string) error
	DoLogout(w http.ResponseWriter, r *http.Request) error
	PageAuth(next http.Handler) http.Handler
	OverridePagAuth(provider Provider) func(next http.Handler) http.Handler
	ApiAuth(next http.Handler) http.Handler
	OverrideApiAuth(provider Provider) func(next http.Handler) http.Handler
}