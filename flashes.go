package auth

import "net/http"

type Flashes interface {
	SetErrorFlash(w http.ResponseWriter, r *http.Request, message string)
	SetSuccessFlash(w http.ResponseWriter, r *http.Request, message string)
}