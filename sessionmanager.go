package auth

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gorilla/sessions"
)

const (
	sessionKey    = "session"
	sessionID     = "sessionId"
	authorization = "Authorization"
	bearer        = "Bearer "
)

var TokenUnavailable = errors.New("Token Unavailable")
var TokenUnrecognized = errors.New("Token Unrecognized")
var AuthFailed = errors.New("Auth failed")

type SessionManager struct {
	store      sessions.Store
	provider   Provider
	redirect   string
	flashes    Flashes
	sessionKey string
}

func NewSessionManager(store sessions.Store, provider Provider, redirect string, flashes Flashes) Manager {
	return &SessionManager{store: store,
		provider:   provider,
		redirect:   redirect,
		flashes:    flashes,
		sessionKey: sessionKey,
	}
}

func NewSessionManagerOverrideSessionKey(store sessions.Store, provider Provider, redirect string, flashes Flashes, overrideSessionKey string) Manager {
	return &SessionManager{store: store,
		provider:   provider,
		redirect:   redirect,
		flashes:    flashes,
		sessionKey: overrideSessionKey,
	}
}

func (s *SessionManager) DoLogin(w http.ResponseWriter, r *http.Request, token string) error {
	sess, sessErr := s.store.Get(r, s.sessionKey)
	if sessErr != nil {
		return sessErr
	}

	sess.Values[sessionID] = token
	sess.Options.MaxAge = 60 * 60 * 2
	return sess.Save(r, w)
}

func (s *SessionManager) DoLogout(w http.ResponseWriter, r *http.Request) error {
	sess, sessErr := s.store.Get(r, s.sessionKey)
	if sessErr != nil {
		return sessErr
	}

	sess.Options.MaxAge = -1
	return sess.Save(r, w)
}

func (s *SessionManager) PageAuth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		authErr := s.guardSession(w, r, s.provider)
		if authErr != nil {
			s.flashes.SetErrorFlash(w, r, r.RequestURI+" requires authentication")
			http.Redirect(w, r, s.redirect, http.StatusSeeOther)
			return
		}

		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (s *SessionManager) OverridePagAuth(provider Provider) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			authErr := s.guardSession(w, r, provider)
			if authErr != nil {
				s.flashes.SetErrorFlash(w, r, r.RequestURI+" requires authentication")
				http.Redirect(w, r, s.redirect, http.StatusSeeOther)
				return
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (s *SessionManager) ApiAuth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		sessionErr := s.guardSession(w, r, s.provider)
		headerErr := s.guardHeader(r, s.provider)

		if sessionErr != nil && headerErr != nil {
			http.Error(w, "Auth required", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (s *SessionManager) OverrideApiAuth(provider Provider) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			sessionErr := s.guardSession(w, r, provider)
			headerErr := s.guardHeader(r, provider)

			if sessionErr != nil && headerErr != nil {
				http.Error(w, "Auth required", http.StatusForbidden)
				return
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (s *SessionManager) guardSession(w http.ResponseWriter, r *http.Request, p Provider) error {
	sessToken, sessErr := s.getSessionToken(w, r)
	if sessToken != "" && sessErr == nil {
		valid, err := p.IsValid(sessToken, r)
		if !valid || err != nil {
			return AuthFailed
		}
		return nil
	}

	return AuthFailed
}

func (s *SessionManager) guardHeader(r *http.Request, p Provider) error {
	headerToken, headerErr := s.getHeaderToken(r)
	if headerToken != "" && headerErr == nil {
		valid, err := p.IsValid(headerToken, r)
		if !valid || err != nil {
			return AuthFailed
		}
		return nil
	}

	return AuthFailed
}

func (s *SessionManager) getSessionToken(w http.ResponseWriter, r *http.Request) (string, error) {
	sess, sessErr := s.store.Get(r, s.sessionKey)
	if sessErr != nil {
		return "", sessErr
	}

	token, tokenOK := sess.Values[sessionID]
	if !tokenOK {
		return "", TokenUnavailable
	}

	tokenString, tokenIsStr := token.(string)
	if !tokenIsStr {
		return "", TokenUnrecognized
	}

	sess.Options.MaxAge = 60 * 60 * 2
	sess.Save(r, w)
	return tokenString, nil
}

func (s *SessionManager) getHeaderToken(r *http.Request) (string, error) {

	raw := r.Header.Get(authorization)
	if raw == "" {
		return "", TokenUnavailable
	}

	token := strings.Replace(raw, bearer, "", 1)
	return token, nil
}
