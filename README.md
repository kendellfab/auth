# auth

A wrapper around gorilla sessions to help manage authentication.

## Provider
The provider is something that will live in the consumers code.  Therefore this is just an interface.

If you want to maintain anything on the request context, you can do it.

type key int

const (
	ContextKey = key(22)
)

func (p ProviderImpl) setUser(usr User, r *http.Request) {
    ctx := r.Context()
    ctx = context.WithValue(ctx, ContextKey, usr)
    *r = *(r.WithContext(ctx))
}

// Then to get this from the context:

func FromContext(ctx context.Context) (User, bool) {
	usr, ok := ctx.Value(ContextKey).(User)
	return usr, ok
}