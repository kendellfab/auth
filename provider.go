package auth

import "net/http"

type Provider interface {
	IsValid(token string, r *http.Request) (bool, error)
}
